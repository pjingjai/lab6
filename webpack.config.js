let path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
   entry: {
       main: './src/app/app.js'
   },
   output: {
       filename: '[name].[chunkhash].js',
       path: path.resolve(__dirname, 'dist')
   },
   plugins: [
       new CleanWebpackPlugin(),
       new HtmlWebpackPlugin({
           template: './src/index.html'
       })
   ],
   devtool: 'inline-source-map',
   devServer: {
       contentBase: './dist',
       port: 8080
   }
};
